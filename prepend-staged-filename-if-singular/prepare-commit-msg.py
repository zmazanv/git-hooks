#!/usr/bin/env python


import os
import subprocess
import sys

commit_msg_filepath: str = os.path.realpath(sys.argv[1])
commit_src: str | None = sys.argv[2] if len(sys.argv) > 2 else None
sha1: str | None = sys.argv[3] if len(sys.argv) > 3 else None


def get_subprocess_stdout(*args: str) -> str:
    """
    Return, as text, the output of the passed arguments joined as a shell command.
    Return an empty string if nothing is passed.
    """
    return (
        ""
        if len(args) == 0
        else subprocess.run(args, capture_output=True, text=True).stdout.strip()
    )


def greedily_remove_prefix(string: str, prefix: str) -> str:
    """
    Return a copy of the passed string with the passed prefix greedily removed.
    """
    while string.startswith(prefix):
        string = string.removeprefix(prefix)

    return string


def exit_if_rebasing() -> None:
    """
    Determine if the git repo is actively rebasing and exit if so.
    """
    stdout: str = get_subprocess_stdout("git", "rev-parse", "--show-toplevel")

    if not stdout:
        sys.exit(1)

    top_level_path: str = os.path.realpath(stdout)
    git_dirpath: str = os.path.join(top_level_path, ".git")
    rebase_dirpaths = (
        os.path.join(git_dirpath, rebase_dir)
        for rebase_dir in ("rebase-apply", "rebase-merge")
    )

    for dir in rebase_dirpaths:
        if os.path.exists(dir):
            sys.exit(0)


def get_staged_files() -> list[str]:
    """
    Return all staged files as a list of filenames.
    """
    return get_subprocess_stdout("git", "diff", "--staged", "--name-only").splitlines()


def get_file_content(filepath: str) -> str:
    """
    Parse the file and return its content as a string.
    """
    if not os.path.isfile(filepath):
        sys.exit(1)

    with open(filepath, "r") as f:
        return f.read()


def main() -> int:
    exit_if_rebasing()

    staged_files: list[str] = get_staged_files()
    if len(staged_files) == 0:
        return 0

    filename: str = staged_files[0]
    prefix: str = f"{filename}: "
    msg: str = greedily_remove_prefix(get_file_content(commit_msg_filepath), prefix)
    new_msg: str = prefix + msg

    with open(commit_msg_filepath, "w") as f:
        f.write(new_msg)

    return 0


if __name__ == "__main__":
    sys.exit(main())
