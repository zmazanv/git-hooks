#!/usr/bin/env bash

__COMMIT_MSG_FILE=$1
__COMMIT_SOURCE=$2
__SHA1=$3

__GIT_DIR="$(git rev-parse --show-toplevel)/.git"
[[ -d "${__GIT_DIR}/rebase-apply" ]] && exit 0
[[ -d "${__GIT_DIR}/rebase-merge" ]] && exit 0

readarray -t __FILES < <(git diff --staged --name-only)

[[ "${#__FILES[@]}" -ne 1 ]] && exit 0

__MSG="$(cat "${__COMMIT_MSG_FILE}")"
__FILENAME_PREFIX="${__FILES[0]}: "

echo "${__FILENAME_PREFIX}${__MSG##"${__FILENAME_PREFIX}"}" >"${__COMMIT_MSG_FILE}"

exit 0
